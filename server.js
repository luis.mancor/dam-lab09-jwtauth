
require("./config/config");
const express = require("express");
const mongoose = require("mongoose");
const app = express();
const bodyParser = require("body-parser");
const path = require('path');
//crea urlencoded app
app.use(bodyParser.urlencoded({urlencoded: false}));
//crea app json parser
app.use(bodyParser.json());
//use of index routes
app.use(require("./routes/index"));
//use of pug templates
app.set('view engine', 'pug');
//use of custom css
app.use(express.static(path.join(__dirname, 'public')));

/*
mongoose.connect('mongodb://localhost:27017/lab07DB', (err, res) =>{
    if(err) throw err;
    console.log('Base de datos Online')
});
*/
//Connection to mongo Atlas
mongoose.connect(
    process.env.URLDB,
    { useNewUrlParser: true, useCreateIndex:true, useUnifiedTopology: true },
    (err, res) => {
        if (err) throw err;
        console.log("Base de datos atlas online");
    }  
);


app.listen(process.env.PORT, () => 
    console.log(`escuchando en puerto ${process.env.PORT}`)
    );


    