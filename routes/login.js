const express = require("express");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const Usuario = require("../models/usuario");
const {verificaToken} = require("../middlewares/authentication"); //function for token validation

if (typeof localStorage === "undefined" || localStorage === null) {
    var LocalStorage = require('node-localstorage').LocalStorage;
    localStorage = new LocalStorage('./scratch');
}

const app = express();

app.get("/", function(req,res){
    try{
        var token = localStorage.getItem('myToken');
        var nombre = localStorage.getItem('nombre');
    }catch(err){
        console.log('No existe token');
    }
    res.render('index',{
        data:token,
        nombre:nombre,
    })
});

app.get("/login", function(req,res){
    res.render('login')
});

app.post("/login", (req, res) => {
    let body = req.body;

    Usuario.findOne({email: body.email}, (err, usuarioDB) => {
        //Control de error
        if (err){
            return res.status(500).json({
                ok:false,
                err,
            });
        }
        //Control de usuario en blanco
        if(!usuarioDB){
            return res.status(400).json({
                ok:false,
                err:{
                    message: "(Usuario) o contraseña incorrectas",
                },
            });
        }
        //Control de que las contraseñas no son iguales
        if(!bcrypt.compareSync(body.password, usuarioDB.password)){
            return res.status(400).json({
                ok: false,
                err:{
                    message: "Usuario o (contraseña) incorrectos",
                },
            });
        }
        //Generacion de token y paso al json
        let token = jwt.sign({usuario: usuarioDB}, process.env.SEED, {expiresIn: process.env.CADUCIDAD_TOKEN})

        localStorage.setItem('myToken', token);
        localStorage.setItem('usuarioID', usuarioDB.email);
        localStorage.setItem('nombre', usuarioDB.nombre);
        /*
        res.json({
            ok: true,
            usuario: usuarioDB,
            token,
        });
        */
        res.render('welcome',{
            data: usuarioDB,
            nombre:usuarioDB.nombre,
        });
        console.log(usuarioDB);
    });
});

app.get("/welcome", verificaToken ,function(req,res){
    try{
        var token = localStorage.getItem('myToken');
        var nombre = localStorage.getItem('nombre');
    }catch(err){
        console.log('No existe token');
    }
    res.render('welcome',{
        data:token,
        nombre:nombre,
    })
});

app.get("/logout", function(req,res){
    localStorage.removeItem('myToken');
    localStorage.removeItem('usuarioID');
    localStorage.removeItem('nombre');
    res.redirect('/login');
});


module.exports = app;

/*
function checkLogin(req,res,next){
    var myToken = localStorage.getItem('myToken');
    try{
        jwt.verify(myToken, process.env.SEED);
    }catch(err){
        return res.status(500).json({
            ok:false,
            err,
        });
    }
    next();
}
*/