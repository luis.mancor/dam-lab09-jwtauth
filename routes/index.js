const express = require("express");
const app = express();
//const router = express.Router();
const usuario = require("./usuario");


app.use(usuario);
app.use(require("./login"));
app.use(require("./pet"));


module.exports = app