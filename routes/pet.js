const express = require("express");
const Pet = require("../models/pets");  //persistency use for model PET
const {verificaToken} = require("../middlewares/authentication"); //function for token validation
const app = express();
const mongoose = require("mongoose");
const bodyParser = require("body-parser"); //allows post request
const methodOverride = require("method-override"); //dependency for put delete request instalation via npm install method-override

app.use(methodOverride('_method'));  //USE OF METHODOVERRIDE

if (typeof localStorage === "undefined" || localStorage === null) {  //Dependency that allows use of localstorage similar to cookie and session handling
    var LocalStorage = require('node-localstorage').LocalStorage;
    localStorage = new LocalStorage('./scratch');
}

//-----------------------PET REGISTRATION--------------------------//
app.get("/petRegistration",verificaToken, function(req,res){
    res.render('petRegister')
})

app.post("/pet",verificaToken, function (req, res){  
    let body = req.body;
    var userID = localStorage.getItem('usuarioID');

    let pet = new Pet({
        nombre: body.nombre,
        edadmascota:body.edadmascota,
        ubicacion: body.ubicacion,
        tamano: body.tamano,
        sexo:body.sexo,
        estado: body.estado,
        historia: body.historia,
        usuarioSubmmited: userID,
    });

    pet.save((err, petDB) => {
        if (err){
            return res.status(400).json({
                ok: false,
                err,
            });
        }
        /*
        res.json({
            ok: true,
            pet: petDB,
        });
        */
        res.redirect('/pet');
        console.log(petDB);
    });
});

//-----------------------PET LISTINGS--------------------------//
app.get("/pet",verificaToken, function (req, res){
    Pet.find({}).exec((err, pets) => {
        if (err){
            return res.status(400).json({
                ok: false,
                err,
            });
        }
        /*
        res.json({
            ok: true,
            pets,
        });
        */
        res.render('petList',{
            "data":pets,
        });
        console.log(pets);

    });
});

//-----------------------PET EDITING--------------------------//
app.get("/petEdit/:id",verificaToken ,async function (req,res){
    let id = req.params.id;
    const pet =  await Pet.findById(req.params.id);
    console.log(pet);
    console.log(pet.historia);
    res.render('petEdit',{
        pet, 
    });
});

app.put("/pet/:id",verificaToken, function (req, res) {
    let id = req.params.id;
    let body = req.body;
    console.log(body);

    Pet.findByIdAndUpdate(id, body, {new: true}, (err, petDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err,
            });
        }
        res.redirect("/pet");
        /*
        res.status(200).json({
            ok: true,
            pet: petDB,
        });
        */

    });
});

//-----------------------PET DELETION--------------------------//
app.delete("/pet/:id",verificaToken ,function(req, res){

    let id = req.params.id;
    Pet.findByIdAndDelete(id, (err, petBorrada)=>{
        if (err){
            return res.status(400).json({
                ok:false,
                err,
            });
        }
        console.log(petBorrada);
        res.redirect('/pet');
        /*
        res.json({
            ok:true,
            pet: petBorrada,
        });
        */
    });
});

module.exports = app;