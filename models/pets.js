const mongoose = require("mongoose");

let Schema = mongoose.Schema;

let estado = {
    values: [
        'ADOPTED',
        'NON_ADOPTED'
    ],
    message: '{VALUE] no es un rol valido'
}

let sexo = {
    values: [
        'HEMBRA',
        'MACHO',
    ],
    message: '{VALUE] no es un rol valido'
}

let size = {
    values: [
        'Pequeño',
        'Mediano',
        'Grande',
    ],
    message: '{VALUE] no es un rol valido'
}

let petSchema = new Schema({
    nombre:{
        type: String,
        required: [true, "El nombre es necesario"],
    },
    edadmascota: {
        type:String,
    },
    ubicacion:{
        type: String,
        required: [true, "La ciudad de residencia es necesaria"],
    },
    tamano: {
        type:String,
        required: [true, "El tamaño es necesario"],
    },
    estado: {
        type: String,
        default: 'NON_ADOPTED',
        enum: estado,
    },
    sexo: {
        type: String,
        required: [true, "El sexo es necesario"],
    },
    historia:{
        type:String,

    },
    usuarioSubmmited:{
        type:String,
    },
});

module.exports = mongoose.model('Pet', petSchema)